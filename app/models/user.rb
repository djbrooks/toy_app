class User < ApplicationRecord
  has_many :microposts
  validates :name, length: {minimum: 2}, presence: true
  validates :email, presence: true
end
